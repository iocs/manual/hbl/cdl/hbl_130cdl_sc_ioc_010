###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##                                                                                          ## 
##  CMDS interlocks handling                                                                ##
##                                                                                          ##  
##                             DIO - Single signal Input/Output                             ##
##                                                                                          ##  
##                                                                                          ##  
############################ Version: 1.0             ########################################
# Author:  Dominik Domagala
# Date:    11.09.2024
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

add_digital("OpMode_Auto",          ARCHIVE=True, PV_DESC="Operation Mode Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Forced",        ARCHIVE=True, PV_DESC="Operation Mode Manual",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("OFF",                  ARCHIVE=True, PV_DESC="Signal OFF",                PV_ONAM="True",           PV_ZNAM="False")
add_digital("ON",                   ARCHIVE=True, PV_DESC="Signal ON",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("SignalForced",         ARCHIVE=True, PV_DESC="Signal forced indicator",   PV_ONAM="True",           PV_ZNAM="False")

#Alarm signals
add_major_alarm("Alarm","Alarm",           PV_ZNAM="NominalState")
add_minor_alarm("Warning","Warning",       PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Forced",              PV_DESC="CMD: Forced Mode")

add_digital("Cmd_ForceOFF",            PV_DESC="CMD: force OFF")
add_digital("Cmd_ForceON",           PV_DESC="CMD: force ON")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Signal Name and Description
add_string("SignalName", 30, PV_VAL="[PLCF#SignalName0]", PV_PINI="YES")
add_string("SignalDesc", 30, PV_VAL="[PLCF#SignalDescription0]", PV_PINI="YES")

#VacOK Setpoint
add_analog("PresR",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="pressure input")
add_analog("TempR",           "REAL", PV_PREC="2",         ARCHIVE=True,              PV_DESC="temperature input")



#External signals

add_digital("Ext_Reserve_01",          ARCHIVE=True,       PV_DESC="external reserve signal 1",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Ext_Reserve_02",          ARCHIVE=True,       PV_DESC="external reserve signal 2",         PV_ONAM="True",           PV_ZNAM="False")
add_digital("Ext_Reserve_03",          ARCHIVE=True,       PV_DESC="external reserve signal 3",         PV_ONAM="True",           PV_ZNAM="False")


